args@{ overlays ? [ ] }:
let
  sources = import ./sources.nix;
  myOverlay = self: super: {
    myRust = super.latest.rustChannels.nightly.rust.override {
      targets = [ "x86_64-unknown-linux-musl" ];
    };
    myRustPlatform = super.rust.makeRustPlatform {
      cargo = self.myRust;
      rustc = self.myRust;
    };
  };
in import sources.nixpkgs (args // {
  overlays = overlays ++ [ (import sources.nixpkgs-mozilla) myOverlay ];
})
