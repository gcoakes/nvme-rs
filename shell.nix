{ pkgs ? import ./nix { } }:
with pkgs;
let linker = pkgs.pkgsStatic.buildPackages.llvmPackages_10;
in mkShell {
  buildInputs =
    [ myRust cargo-edit cargo-expand maturin python3 gitlab-runner ];
  CARGO_TARGET_X86_64_UNKNOWN_LINUX_MUSL_LINKER =
    "${pkgs.pkgsCross.musl64.buildPackages.llvmPackages_10.lld}/bin/lld";
  shellHook = ''
    if [ ! -d .venv/ ]; then
      python -m venv .venv
      . ./.venv/bin/activate
      pip install -r nvme-core/dev-requirements.txt
    else
      . ./.venv/bin/activate
    fi
  '';
}
