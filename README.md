# nvme-rs

_Both the underlying library and the CLI tool are currently under
development. Expect changes in the API and CLI interface._

nvme-rs is a cross platform tool for interacting with NVMe drives from
the command line. While this project is merely in the proof of concept
phase, the end-goal is to have parity with the functionality provided by
[nvme-cli](https://github.com/linux-nvme/nvme-cli), to extend platform
support to Windows, and to expose an official API.

Similar to nvme-cli, nvme-rs takes particular care to keep its
terminology consistent with that of the [NVMe
specification](https://nvmexpress.org/resources/specifications/).

## Interoperability

Every NVMe operation implemented by nvme-rs will expose both a human
readable output and a structured output such as json or any format other
supported by [serde](https://serde.rs).

Further, the core library of this tool will be exported as a crate with
(eventually) a stable interface. This will allow for interoperability
with other [languages](https://github.com/PyO3/PyO3) and creation of
testing frameworks.

Finally, nvme-rs will expose an interface which will act as a drop-in
replacement for nvme-cli in places where it is currently in use.

## Extensibility

There are currently no concrete plans for vendor specific extensions.

## Licenses

All library crates in this project are licensed under the LGPL license
as described in their individual LICENSE files. All binary crates in
this project are licensed under the GPL license as described in their
individual LICENSE files.
