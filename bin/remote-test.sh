#!/bin/sh
log() {
    if [ -t 1 ]; then
        >&2 printf '\e[1m%s\e[0m\n' "$1"
    else
        >&2 printf '%s\n' "$1"
    fi
}
while [ $# -gt 0 ]; do
    case "$1" in
        --ssh-args) shift
            SSH_ARGS="$1"
            shift
            ;;
        --scp-args) shift
            SCP_ARGS="$1"
            shift
            ;;
        --win*) shift
            WINDOWS=1
            ;;
        --lin*) shift
            WINDOWS=0
            ;;
        -*)
            >&2 echo "Unrecognized argument: $1"
            shift
            ;;
        *)
            REMOTE_SYSTEM="$1"
            shift
            ;;
    esac
done
: "${REMOTE_SYSTEM?No remote system specified.}"
SSH="ssh $SSH_ARGS"
SCP="scp $SCP_ARGS"

PS_SCRIPT="
# Remove everything except the target directory.
\$repo = Join-Path (Resolve-Path .) \"nvme-rs\"

Get-ChildItem -Path \$repo -Recurse \`
| Select -ExpandProperty FullName \`
| ? {\$_ -NotLike (Join-Path \$repo '*target*')} \`
| ? {\$_ -NotLike (Join-Path \$repo '.vscode*')} \`
| Sort-Object -Property Length -Descending \`
| Remove-Item -Force 

Expand-Archive -DestinationPath \$repo -Path \"nvme-rs.zip\"

Remove-Item \"nvme-rs.zip\"

Set-Location -Path \$repo

& cargo +nightly test
"

arfile="$(mktemp --suffix=.zip)"
trap 'test -f "$arfile" && rm "$arfile"' EXIT

commit="$(git stash create -u)"
if [ "$commit" = '' ]; then
    commit='HEAD'
fi
if [ "$WINDOWS" = "1" ]; then
    log "Windows System: $REMOTE_SYSTEM"

    log "Creating archive of repository's current state."
    git archive --format=zip --output="$arfile" "$commit"

    log "Copying archive to remote system."
    $SCP "$arfile" "$REMOTE_SYSTEM:nvme-rs.zip"

    log "Running tests on remote system."
    $SSH "$REMOTE_SYSTEM" "$PS_SCRIPT"
else
    log "Linux System: $REMOTE_SYSTEM"

    log "Removing old repository files."
    $SSH "$REMOTE_SYSTEM" find nvme-rs ! -path '*target*' ! -path 'nvme-rs' -delete

    log "Replicating repository's current state on remote system."
    git archive --format=tar.gz "$commit" --prefix=nvme-rs/ \
    | $SSH "$REMOTE_SYSTEM" tar xz

    log "Running tests on remote system."
    $SSH "$REMOTE_SYSTEM" sh <<EOF
test -d "\$HOME/.cargo/bin" && export PATH="\$HOME/.cargo/bin:\$PATH"
cd nvme-rs && cargo +nightly test
EOF
fi
