use std::collections::HashSet;

use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, parse_quote, AttributeArgs, ItemStruct, Lit, NestedMeta, Visibility};

#[proc_macro_attribute]
pub fn pyclass_readonly_all(args: TokenStream, input: TokenStream) -> TokenStream {
    // Parse the input tokens into a syntax tree
    let args = parse_macro_input!(args as AttributeArgs);
    let mut input = parse_macro_input!(input as ItemStruct);

    let skip: HashSet<String> = args
        .into_iter()
        .map(|m| match m {
            NestedMeta::Lit(Lit::Str(s)) => s.value(),
            _ => panic!("Only literal strings allowed."),
        })
        .collect();

    for field in input.fields.iter_mut() {
        if let Visibility::Public(_) = field.vis {
            if field
                .ident
                .as_ref()
                .map_or(false, |i| !skip.contains(&i.to_string()))
            {
                field.attrs.push(parse_quote! {#[pyo3(get)]});
            }
        }
    }

    let expanded = quote! {
        #[pyclass]
        #input
    };

    expanded.into()
}
