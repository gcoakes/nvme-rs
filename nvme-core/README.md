# nvme-core

A cross-platform library which aims to provide access to low-level functions of NVMe devices.
The goal is to have feature parity with nvme-cli on Linux and also bring that same functionality to Windows.
This library in addition to low-level access aims to provide interpretted data structures in a manner which is accessible from other libraries and programs.
