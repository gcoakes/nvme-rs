/*
 * Copyright (C) 2020  Oakes, Gregory <gregoryoakes@fastmail.com>
 * Author: Oakes, Gregory <gregory.oakes@fastmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

mod common;

use std::path::PathBuf;

use nvme_core::{
    models::{IDCtrl, IDNamespace},
    Device,
};

#[test]
fn query_identify_ctrl() -> Result<(), Box<dyn std::error::Error>> {
    for path in common::get_device_paths() {
        let mut dev = Device::open(PathBuf::from(path))?;
        assert!(dev.query::<IDCtrl>().is_ok());
    }
    Ok(())
}

#[test]
fn query_identify_ns() -> Result<(), Box<dyn std::error::Error>> {
    for path in common::get_device_paths() {
        let mut dev = Device::open(PathBuf::from(path))?;
        assert!(dev.query_ns::<IDNamespace>(1).is_ok());
    }
    Ok(())
}
