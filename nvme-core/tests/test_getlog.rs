/*
 * Copyright (C) 2020  Oakes, Gregory <gregoryoakes@fastmail.com>
 * Author: Oakes, Gregory <gregory.oakes@fastmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

mod common;

use std::path::PathBuf;

use nvme_core::{
    models::{logpage::*, IDCtrl},
    Device, ErrorLogIterator,
};

#[test]
fn query_error_log() -> Result<(), Box<dyn std::error::Error>> {
    for path in common::get_device_paths() {
        let mut dev = Device::open(PathBuf::from(path))?;

        let id_ctrl = dev.query::<IDCtrl>().unwrap();

        let errs: Vec<ErrorLogEntry> = dev
            .query_iter::<ErrorLogEntry, ErrorLogIterator>(0, id_ctrl.elpe as usize)
            .collect();

        assert!(errs.len() == id_ctrl.elpe as usize)
    }
    Ok(())
}

#[test]
fn query_smart_log() -> Result<(), Box<dyn std::error::Error>> {
    for path in common::get_device_paths() {
        let mut dev = Device::open(PathBuf::from(path))?;

        assert!(dev.query::<SmartLog>().is_ok());
    }
    Ok(())
}

#[cfg_attr(windows, ignore = "broken on windows for now")]
#[test]
fn query_fw_slot_log() -> Result<(), Box<dyn std::error::Error>> {
    for path in common::get_device_paths() {
        let mut dev = Device::open(PathBuf::from(path))?;

        assert!(dev.query::<FwSlotLog>().is_ok());
    }
    Ok(())
}

#[test]
fn query_cmd_effect_log() -> Result<(), Box<dyn std::error::Error>> {
    for path in common::get_device_paths() {
        let mut dev = Device::open(PathBuf::from(path))?;

        assert!(dev.query::<CmdEffectLog>().is_ok());
    }
    Ok(())
}
