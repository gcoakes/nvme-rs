/*
 * Copyright (C) 2020  Oakes, Gregory <gregoryoakes@fastmail.com>
 * Author: Oakes, Gregory <gregory.oakes@fastmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#[cfg(windows)]
use std::{process::Command, str::FromStr};

#[cfg(windows)]
pub fn get_device_paths() -> Vec<String> {
    String::from_utf8(
            Command::new("powershell")
                .arg("Get-Disk | ? IsBoot -EQ $False | ? BusType -EQ 'NVMe' | % {\"\\\\.\\PHYSICALDRIVE$($_.Number)\"}")
                .output().unwrap()
                .stdout,
        ).unwrap().lines().map(|l| String::from_str(l).unwrap()).collect()
}

#[cfg(target_os = "linux")]
pub fn get_device_paths() -> Vec<String> {
    std::fs::read_dir("/dev")
        .unwrap()
        .filter_map(|dev| {
            let s = dev.unwrap().path().into_os_string().into_string().unwrap();
            if s.starts_with("/dev/nvme") {
                Some(s)
            } else {
                None
            }
        })
        .collect()
}
