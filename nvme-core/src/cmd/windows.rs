/*
 * Copyright (C) 2020  Oakes, Gregory <gregoryoakes@fastmail.com>
 * Author: Oakes, Gregory <gregory.oakes@fastmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

/// Windows specific NVMe device operations.
///
/// Windows' userspace NVMe interface is much more constrained in comparison to Linux IOCTLs. It
/// requires the kernel to keep track of changes for NVMe devices. Passthrough commands are only
/// possible if the device supports the command effect log.
use std::{convert::TryInto, mem, os::windows::io::AsRawHandle, ptr};

use crate::{
    cmd::{CommandBuilder, CommandData, CommandPassthru, OperationType, PassthruResult},
    opcode::{AdminOpcode, SpecAdminOpcode},
    Device, Status,
};
use winapi::{
    shared::minwindef::{DWORD, LPDWORD, LPVOID},
    shared::ntdef::HANDLE,
    um::{
        ioapiset::DeviceIoControl,
        winioctl::{
            PropertyStandardQuery, StorageAdapterProtocolSpecificProperty,
            StorageDeviceProtocolSpecificProperty, IOCTL_STORAGE_QUERY_PROPERTY,
            STORAGE_PROPERTY_ID, STORAGE_QUERY_TYPE,
        },
    },
};

pub struct WindowsCommand(CommandBuilder);

impl From<CommandBuilder> for WindowsCommand {
    fn from(builder: CommandBuilder) -> Self {
        Self(builder)
    }
}

fn protocol_specific_query(
    device: &mut Device,
    mut query: ProtocolSpecificDataQuery,
) -> PassthruResult {
    let mut buf: Vec<u8> = vec![
        0u8;
        mem::size_of::<ProtocolSpecificDataDesc>()
            + query.data.protocol_data_length as usize
    ];
    let mut ret_size: DWORD = 0;
    let status = unsafe {
        DeviceIoControl(
            // hDevice: The Windows HANDLE object.
            device.fd.as_raw_handle() as HANDLE,
            // dwIoControlCode: The IOCTL to issue to the device.
            IOCTL_STORAGE_QUERY_PROPERTY,
            // lpInBuffer: The void pointer to the input buffer.
            (&mut query as *mut ProtocolSpecificDataQuery) as LPVOID,
            // nInBufferSize: The size of the input buffer.
            mem::size_of::<ProtocolSpecificDataQuery>()
                .try_into()
                .unwrap(),
            // lpOutBuffer: The void pointer to the output buffer.
            buf.as_mut_ptr() as LPVOID,
            // nOutBufferSize: The size of the output buffer.
            buf.len() as DWORD,
            // lpBytesReturned: The void pointer to memory which should be filled with
            // the number of bytes written to the output buffer.
            &mut ret_size as LPDWORD,
            // lpOverlapped: The overlapped pointer used for asynchronous IOCTL calls.
            ptr::null_mut(),
        )
    };
    if status == 0 {
        Err(std::io::Error::last_os_error())
    } else {
        let mut ret_buf = buf.split_off(mem::size_of::<ProtocolSpecificDataDesc>());
        ret_buf.truncate((ret_size - mem::size_of::<ProtocolSpecificDataDesc>() as u32) as usize);
        Ok((
            Status::from_windows(status),
            Some(ret_buf.into_boxed_slice()),
        ))
    }
}

impl CommandPassthru for WindowsCommand {
    /// Allocate the buffer for the DeviceIoControl and issue the command. This translates the
    /// actual passthrough command into what the Windows API recognizes. This behaviors disallows
    /// using IOCTL_STORAGE_PROTOCOL_COMMAND for commands that can otherwise be queried using
    /// structured Windows APIs. This is becuase the Windows documentation specifically recommends
    /// to only use that IOCTL for vendor specific commands. For more information, refer to the
    /// [Windows
    /// documentation](https://docs.microsoft.com/en-us/windows/win32/fileio/working-with-nvme-devices)
    /// on working with NVMe devices.
    fn passthru(self: Self, device: &mut Device) -> PassthruResult {
        match self.0.operation {
            OperationType::AdminCmd(AdminOpcode::SpecOpcode(SpecAdminOpcode::Identify)) => {
                let len: DWORD = match self.0.data {
                    Some(CommandData::Length(len)) => len.try_into().unwrap(),
                    dt => panic!(
                        "Invalid command data type for an identify command: {:?}",
                        dt
                    ),
                };
                let cns = self.0.cdw10 & 0xff;
                protocol_specific_query(
                    device,
                    ProtocolSpecificDataQuery {
                        property_id: if cns == 0 {
                            StorageDeviceProtocolSpecificProperty
                        } else {
                            StorageAdapterProtocolSpecificProperty
                        },
                        query_type: PropertyStandardQuery,
                        data: ProtocolSpecificData {
                            protocol_type: ProtocolType::Nvme,
                            data_type: NvmeDataType::Identify,
                            protocol_data_request_value: cns as DWORD,
                            protocol_data_request_sub_value: if cns == 0 { self.0.nsid } else { 0 },
                            protocol_data_offset: mem::size_of::<ProtocolSpecificData>() as DWORD,
                            protocol_data_length: len as DWORD,
                            fixed_protocol_return_data: 0,
                            protocol_data_request_sub_value2: 0,
                            protocol_data_request_sub_value3: 0,
                            reserved: 0,
                        },
                    },
                )
            }
            OperationType::AdminCmd(AdminOpcode::SpecOpcode(SpecAdminOpcode::GetLogPage)) => {
                let len: DWORD = match self.0.data {
                    Some(CommandData::Length(len)) => len.try_into().unwrap(),
                    dt => panic!(
                        "Invalid command data type for a get log page command: {:?}",
                        dt
                    ),
                };
                let lid: DWORD = self.0.cdw10 & 0xff;
                protocol_specific_query(
                    device,
                    ProtocolSpecificDataQuery {
                        property_id: match lid {
                            0x01 | 0x03..=0x0f => StorageAdapterProtocolSpecificProperty,
                            // Fallback: assume it's an adapter (controller) scoped command, if nsid
                            // is 0. Otherwise, assume it's a device (namespace) scoped command.
                            _ => {
                                if self.0.nsid == 0 {
                                    StorageAdapterProtocolSpecificProperty
                                } else {
                                    StorageDeviceProtocolSpecificProperty
                                }
                            }
                        },
                        query_type: PropertyStandardQuery,
                        data: ProtocolSpecificData {
                            protocol_type: ProtocolType::Nvme,
                            data_type: NvmeDataType::LogPage,
                            protocol_data_request_value: lid,
                            protocol_data_request_sub_value: self.0.nsid,
                            protocol_data_offset: mem::size_of::<ProtocolSpecificData>() as DWORD,
                            protocol_data_length: len as DWORD,
                            fixed_protocol_return_data: 0,
                            protocol_data_request_sub_value2: 0,
                            protocol_data_request_sub_value3: 0,
                            reserved: 0,
                        },
                    },
                )
            }
            unhandled => panic!("{:?} is not implemented yet", unhandled),
        }
    }
}

#[derive(Debug, Copy, Clone)]
#[repr(C)]
struct ProtocolSpecificDataQuery {
    property_id: STORAGE_PROPERTY_ID,
    query_type: STORAGE_QUERY_TYPE,
    data: ProtocolSpecificData,
}

#[derive(Debug, Copy, Clone)]
#[repr(C)]
struct ProtocolSpecificDataDesc {
    version: DWORD,
    size: DWORD,
    data: ProtocolSpecificData,
}

#[derive(Debug, Copy, Clone)]
#[repr(C)]
struct ProtocolSpecificData {
    protocol_type: ProtocolType,
    data_type: NvmeDataType,
    protocol_data_request_value: DWORD,
    protocol_data_request_sub_value: DWORD,
    protocol_data_offset: DWORD,
    protocol_data_length: DWORD,
    fixed_protocol_return_data: DWORD,
    protocol_data_request_sub_value2: DWORD,
    protocol_data_request_sub_value3: DWORD,
    reserved: DWORD,
}

#[allow(dead_code)]
#[derive(Debug, Copy, Clone)]
#[repr(u32)]
enum ProtocolType {
    Unknown = 0x00,
    Scsi,
    Ata,
    Nvme,
    Sd,
    Ufs,
    Proprietary = 0x7e,
    MaxReserved = 0x7f,
}

#[allow(dead_code)]
#[derive(Debug, Copy, Clone)]
#[repr(u32)]
enum NvmeDataType {
    Unknown = 0,
    Identify,
    LogPage,
    Feature,
}
