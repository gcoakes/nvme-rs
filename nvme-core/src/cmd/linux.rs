/*
 * Copyright (C) 2019  Oakes, Gregory <gregory.oakes@outlook.com>
 * Author: Oakes, Gregory <gregory.oakes@outlook.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

use crate::{
    cmd::{CommandBuilder, CommandData, CommandPassthru, OperationType, PassthruResult},
    opcode::DataTransferDirection,
    Device, Status,
};
use nix::request_code_readwrite;
use std::{io::Error as IOError, mem, os::unix::io::AsRawFd};

/// Container for a RawLinuxCommand and some associated runtime data.
#[derive(Debug)]
pub struct LinuxCommand {
    command_type: OperationType,
    data_direction_override: Option<DataTransferDirection>,
    data: Option<CommandData>,
    raw_cmd: RawLinuxCommand,
}

/// Rust representation of a raw userspace NVM command for Linux. This structure differs slightly
/// from the NVM Specification's description of it because the Linux Kernel does internal
/// conversions.
#[derive(Default, Debug, Copy, Clone)]
#[repr(C)]
pub struct RawLinuxCommand {
    /// The raw operation code of the command to be executed.
    pub opcode: u8,
    /// Two bitfields packed into a u8. One specifying the fused operation type, if any, and one
    /// specifying the type of data transfer to use.
    pub flags: u8,
    /// *This field is unused from the perspective of userspace.* A unique identifier for the
    /// command when combined with the submission queue identifier.
    pub cid: u16,
    /// This field specifies the namespace that this command applies to. If the namespace
    /// identifier is not used for the command, then this field shall be cleared to 0h. The value
    /// FFFFFFFFh in this field is a broadcast value, where the scope (e.g., the NVM subsystem, all
    /// attached namespaces, or all namespaces in the NVM subsystem) is dependent on the command.
    ///
    /// Specifying an inactive namespace identifier in a command that uses the namespace identifier
    /// shall cause the controller to abort the command with status Invalid Field in Command,
    /// unless otherwise specified.  Specifying an invalid namespace identifier in a command that
    /// uses the namespace identifier shall cause the controller to abort the command with status
    /// Invalid Namespace or Format, unless otherwise specified.
    ///
    /// If the namespace identifier is used for the command, the value FFFFFFFFh is not supported
    /// for that command, and the host specifies a value of FFFFFFFFh, then the controller should
    /// abort the command with status Invalid Field in Command, unless otherwise specified.
    ///
    /// If the namespace identifier is not used for the command and the host specifies a value from
    /// 1h to FFFFFFFEh, then the controller should abort the command with status Invalid Field in
    /// Command, unless otherwise
    pub nsid: u32,
    /// Reserved by the NVMe specification.
    pub cdw2: u32,
    /// Reserved by the NVMe specification.
    pub cdw3: u32,
    /// A raw pointer to an allocated metadata buffer. The length of this buffer should correspond
    /// directly with the metadata_size field. This differs from the NVMe specification in that the
    /// Linux Kernel will translate this from a user space buffer into what the NVMe specification
    /// requires in the MPTR field.
    pub metadata: u64,
    /// A raw pointer to an allocated data buffer. The length of this buffer should correspond
    /// directly with the data_size field. This differs from the NVMe specification in that the
    /// Linux Kernel will translate this from a user space buffer into what the NVMe specification
    /// requires in the DPTR field.
    pub addr: u64,
    /// The length of the metadata buffer in bytes.
    pub metadata_size: u32,
    /// The length of the data buffer in bytes.
    pub data_size: u32,
    pub cdw10: u32,
    pub cdw11: u32,
    pub cdw12: u32,
    pub cdw13: u32,
    pub cdw14: u32,
    pub cdw15: u32,
    pub timeout_ms: u32,
    pub result: u32,
}

impl From<CommandBuilder> for LinuxCommand {
    fn from(builder: CommandBuilder) -> Self {
        let raw_meta_size = match builder.metadata_size {
            Some(size) => size,
            None => 0,
        };
        LinuxCommand {
            command_type: builder.operation,
            data_direction_override: builder.data_direction_override,
            data: builder.data,
            raw_cmd: RawLinuxCommand {
                flags: builder.flags,
                nsid: builder.nsid,
                metadata_size: raw_meta_size,
                cdw10: builder.cdw10,
                cdw11: builder.cdw11,
                cdw12: builder.cdw12,
                cdw13: builder.cdw13,
                cdw14: builder.cdw14,
                cdw15: builder.cdw15,
                timeout_ms: builder.timeout.as_millis() as u32,
                ..Default::default()
            },
        }
    }
}

impl CommandPassthru for LinuxCommand {
    /// Allocate the buffer for and issue the command. It creates a copy of self so self can be
    /// used multiple times to issue the same command.
    fn passthru(mut self, device: &mut Device) -> PassthruResult {
        let (data_size, mut input_data) = match self.data {
            Some(CommandData::Length(len)) => (len, Some(vec![0; len as usize].into_boxed_slice())),
            Some(CommandData::Input(data)) => (data.len(), Some(data)),
            None => (0, None),
        };
        self.raw_cmd.addr = match input_data.as_mut() {
            Some(data) => data.as_mut_ptr() as u64,
            None => 0,
        };
        self.raw_cmd.data_size = data_size as u32;
        match self.command_type {
            OperationType::AdminCmd(opcode) => {
                self.raw_cmd.opcode = u8::from(opcode);
                unsafe { self.raw_cmd.submit_admin(device) }
            }
        }
        .map(|status| (status, input_data))
    }
}

impl RawLinuxCommand {
    /// Issue self as a raw NVM command to the given device. This is unsafe because it assumes
    /// passes a mutable pointer to an OS native function which may or may not write to self. This
    /// should not be used directly unless there is a specific need. Use passthru for most normal
    /// cases.
    ///
    /// An Ok return does not necessarily mean the command was issued without errors, but rather it
    /// means that the command was issued without OS errors. The status could potentially mean
    /// there were errors within the drive or in the formation of the command.
    unsafe fn submit_admin(&mut self, device: &mut Device) -> Result<Status, IOError> {
        // Issue the raw ioctl.
        let res = libc::ioctl(
            device.fd.as_raw_fd(),
            request_code_readwrite!(b'N', 0x41u8, mem::size_of::<RawLinuxCommand>()),
            self as *mut RawLinuxCommand,
        );

        // The Linux kernel reports OS related errors as negative return codes,
        if res < 0 {
            Err(IOError::from_raw_os_error(-res))

        // and NVM status codes as positive return codes.
        } else {
            Ok(Status::from_linux(res as u16))
        }
    }
}
