/*
 * Copyright (C) 2019  Oakes, Gregory <gregory.oakes@outlook.com>
 * Author: Oakes, Gregory <gregory.oakes@outlook.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#[cfg(target_os = "linux")]
mod linux;
#[cfg(target_os = "linux")]
use crate::cmd::linux::LinuxCommand;

#[cfg(windows)]
mod windows;
#[cfg(windows)]
use crate::cmd::windows::WindowsCommand;

use crate::{
    opcode::{DataTransferDirection, OperationType, SpecAdminOpcode},
    Device, Status,
};
use std::time::Duration;

#[derive(Debug, Clone)]
pub enum CommandData {
    Length(usize),
    Input(Box<[u8]>),
}

#[derive(Debug, Clone)]
pub struct CommandBuilder {
    /// The type of the command to be issued.
    pub operation: OperationType,
    /// If Some, the data transfer direction will not be inferred from the operation, but rather
    /// explicitly set from this field.
    pub data_direction_override: Option<DataTransferDirection>,
    /// Two bitfields packed into a u8. One specifying the fused operation type, if any, and one
    /// specifying the type of data transfer to use.
    pub flags: u8,
    /// This field specifies the namespace that this command applies to. If the namespace
    /// identifier is not used for the command, then this field shall be cleared to 0h. The value
    /// FFFFFFFFh in this field is a broadcast value, where the scope (e.g., the NVM subsystem, all
    /// attached namespaces, or all namespaces in the NVM subsystem) is dependent on the command.
    ///
    /// Specifying an inactive namespace identifier in a command that uses the namespace identifier
    /// shall cause the controller to abort the command with status Invalid Field in Command,
    /// unless otherwise specified.  Specifying an invalid namespace identifier in a command that
    /// uses the namespace identifier shall cause the controller to abort the command with status
    /// Invalid Namespace or Format, unless otherwise specified.
    ///
    /// If the namespace identifier is used for the command, the value FFFFFFFFh is not supported
    /// for that command, and the host specifies a value of FFFFFFFFh, then the controller should
    /// abort the command with status Invalid Field in Command, unless otherwise specified.
    ///
    /// If the namespace identifier is not used for the command and the host specifies a value from
    /// 1h to FFFFFFFEh, then the controller should abort the command with status Invalid Field in
    /// Command, unless otherwise
    pub nsid: u32,
    /// The length of the metadata buffer in bytes.
    pub metadata_size: Option<u32>,
    /// The length of the data buffer in bytes.
    pub data: Option<CommandData>,
    pub cdw10: u32,
    pub cdw11: u32,
    pub cdw12: u32,
    pub cdw13: u32,
    pub cdw14: u32,
    pub cdw15: u32,
    pub timeout: Duration,
}

impl CommandBuilder {
    pub fn new<T>(op: T) -> CommandBuilder
    where
        T: Into<OperationType>,
    {
        CommandBuilder {
            operation: op.into(),
            data_direction_override: None,
            flags: 0,
            nsid: 0,
            metadata_size: None,
            data: None,
            cdw10: 0,
            cdw11: 0,
            cdw12: 0,
            cdw13: 0,
            cdw14: 0,
            cdw15: 0,
            timeout: Duration::from_millis(0),
        }
    }

    pub fn identify(cntid: u16, cns: u8, nvmsetid: u16, uuid_ix: u8, data_len: usize) -> Self {
        Self::new(SpecAdminOpcode::Identify)
            .with_cdw10(cns as u32 | ((cntid as u32) << 16))
            .with_cdw11(nvmsetid as u32)
            .with_cdw14((uuid_ix & 0x3f) as u32)
            .with_data_len(data_len)
    }

    pub fn get_log_page(
        log_id: u8,
        lsp: u8,
        lpo: u64,
        lsi: u16,
        rae: bool,
        uuid_ix: u8,
        data_len: usize,
    ) -> Self {
        let numd = (data_len >> 2) - 1;
        Self::new(SpecAdminOpcode::GetLogPage)
            .with_cdw10(
                log_id as u32
                    | (((numd & 0xffff) as u32) << 16)
                    | if rae { 1 << 15 } else { 0 }
                    | ((lsp as u32) << 8),
            )
            .with_cdw11(((lsi as u32) << 16) | (numd >> 16) as u32)
            .with_cdw12(lpo as u32)
            .with_cdw13((lpo >> 16) as u32)
            .with_cdw14((uuid_ix & 0x3f) as u32)
            .with_data_len(data_len)
    }

    pub fn submit(self, device: &mut Device) -> PassthruResult {
        #[cfg(target_os = "linux")]
        {
            LinuxCommand::from(self).passthru(device)
        }
        #[cfg(windows)]
        {
            WindowsCommand::from(self).passthru(device)
        }
    }

    pub fn submit_get_log_iter(self, device: &mut Device) -> LogPageIter {
        match self.data {
            Some(CommandData::Length(l)) => LogPageIter::new(self, device, l as u64),
            _ => panic!("log page iter called without a data length"),
        }
    }

    pub fn flags(&mut self, flags: u8) {
        self.flags = flags;
    }

    #[inline(always)]
    pub fn with_flags(mut self, flags: u8) -> Self {
        self.flags(flags);
        self
    }

    pub fn nsid(&mut self, nsid: u32) {
        self.nsid = nsid;
    }

    #[inline(always)]
    pub fn with_nsid(mut self, nsid: u32) -> Self {
        self.nsid(nsid);
        self
    }

    pub fn metadata(&mut self, metadata_size: u32) {
        self.metadata_size = Some(metadata_size);
    }

    #[inline(always)]
    pub fn with_metadata(mut self, metadata_size: u32) -> Self {
        self.metadata(metadata_size);
        self
    }

    pub fn data_len(&mut self, data_size: usize) {
        self.data = Some(CommandData::Length(data_size));
    }

    #[inline(always)]
    pub fn with_data_len(mut self, data_size: usize) -> Self {
        self.data_len(data_size);
        self
    }

    pub fn data<T: Into<Box<[u8]>>>(&mut self, data: T) {
        self.data = Some(CommandData::Input(data.into()));
    }

    #[inline(always)]
    pub fn with_data<T: Into<Box<[u8]>>>(mut self, data: T) -> Self {
        self.data(data);
        self
    }

    pub fn cdw10(&mut self, cdw10: u32) {
        self.cdw10 = cdw10;
    }

    #[inline(always)]
    pub fn with_cdw10(mut self, cdw10: u32) -> Self {
        self.cdw10(cdw10);
        self
    }

    pub fn cdw11(&mut self, cdw11: u32) {
        self.cdw11 = cdw11;
    }

    #[inline(always)]
    pub fn with_cdw11(mut self, cdw11: u32) -> Self {
        self.cdw11(cdw11);
        self
    }

    pub fn cdw12(&mut self, cdw12: u32) {
        self.cdw12 = cdw12;
    }

    #[inline(always)]
    pub fn with_cdw12(mut self, cdw12: u32) -> Self {
        self.cdw12(cdw12);
        self
    }

    pub fn cdw13(&mut self, cdw13: u32) {
        self.cdw13 = cdw13;
    }

    #[inline(always)]
    pub fn with_cdw13(mut self, cdw13: u32) -> Self {
        self.cdw13(cdw13);
        self
    }

    pub fn cdw14(&mut self, cdw14: u32) {
        self.cdw14 = cdw14;
    }

    #[inline(always)]
    pub fn with_cdw14(mut self, cdw14: u32) -> Self {
        self.cdw14(cdw14);
        self
    }

    pub fn cdw15(&mut self, cdw15: u32) {
        self.cdw15 = cdw15;
    }

    #[inline(always)]
    pub fn with_cdw15(mut self, cdw15: u32) -> Self {
        self.cdw15(cdw15);
        self
    }

    pub fn timeout(&mut self, timeout: Duration) {
        self.timeout = timeout;
    }

    #[inline(always)]
    pub fn with_timeout(mut self, timeout: Duration) -> Self {
        self.timeout(timeout);
        self
    }
}

pub type PassthruResult = Result<(Status, Option<Box<[u8]>>), std::io::Error>;
pub trait CommandPassthru {
    fn passthru(self, device: &mut Device) -> PassthruResult;
}

pub struct LogPageIter<'a> {
    cmd: CommandBuilder,
    device: &'a mut Device,
    offset: u64,
    target_size: u64,
}

impl<'a> LogPageIter<'a> {
    pub fn new(cmd: CommandBuilder, device: &'a mut Device, target_size: u64) -> Self {
        Self {
            cmd,
            device,
            target_size,
            offset: 0,
        }
    }
}

impl<'a> Iterator for LogPageIter<'a> {
    type Item = PassthruResult;

    fn next(&mut self) -> Option<Self::Item> {
        if self.offset < self.target_size {
            let size = std::cmp::min(self.target_size - self.offset, 4096);
            let mut cmd = self.cmd.clone().with_data_len(size as usize);
            let numd = (size >> 2) - 1;
            cmd.cdw10 = (cmd.cdw10 & 0xffff) | ((numd as u32) << 16);
            cmd.cdw11 &= 0xffff0000;
            cmd.cdw12 = self.offset as u32;
            self.offset += size;
            Some(cmd.submit(self.device))
        } else {
            None
        }
    }
}
