/*
 * Copyright (C) 2019  Oakes, Gregory <gregory.oakes@outlook.com>
 * Author: Oakes, Gregory <gregory.oakes@outlook.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

use std::{
    convert::{From, TryFrom, TryInto},
    mem::transmute,
};

#[derive(Debug, Clone)]
pub enum OperationType {
    AdminCmd(AdminOpcode),
}

impl From<AdminOpcode> for OperationType {
    fn from(value: AdminOpcode) -> Self {
        OperationType::AdminCmd(value)
    }
}

impl From<SpecAdminOpcode> for OperationType {
    fn from(value: SpecAdminOpcode) -> Self {
        AdminOpcode::from(value).into()
    }
}

pub type AdminOpcode = OpcodeBase<SpecAdminOpcode>;
pub type NVMOpcode = OpcodeBase<SpecNVMOpcode>;

/// Abstraction on top of NVM Specification operation codes which allows for distinction between
/// base specification operation codes, vendor specific codes, and theoretically never used
/// reserved codes.
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub enum OpcodeBase<T>
where
    T: TryFrom<u8, Error = UnrecognizedOpcode>,
{
    SpecOpcode(T),
    VendorSpecific(u8),
    Reserved(u8),
}

impl<T> From<u8> for OpcodeBase<T>
where
    T: TryFrom<u8, Error = UnrecognizedOpcode>,
{
    fn from(value: u8) -> Self {
        match value.try_into() {
            Ok(spec_code) => Self::SpecOpcode(spec_code),
            Err(unrec) => Self::from(unrec),
        }
    }
}

impl<T> From<OpcodeBase<T>> for u8
where
    T: TryFrom<u8, Error = UnrecognizedOpcode>,
    u8: From<T>,
{
    fn from(value: OpcodeBase<T>) -> Self {
        match value {
            OpcodeBase::SpecOpcode(code) => u8::from(code),
            OpcodeBase::VendorSpecific(code) => code,
            OpcodeBase::Reserved(code) => code,
        }
    }
}

impl<T> From<UnrecognizedOpcode> for OpcodeBase<T>
where
    T: TryFrom<u8, Error = UnrecognizedOpcode>,
{
    fn from(value: UnrecognizedOpcode) -> Self {
        match value {
            UnrecognizedOpcode::VendorSpecific(code) => OpcodeBase::VendorSpecific(code),
            UnrecognizedOpcode::Reserved(code) => OpcodeBase::Reserved(code),
        }
    }
}

#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub enum UnrecognizedOpcode {
    VendorSpecific(u8),
    Reserved(u8),
}

#[derive(Debug, Clone)]
pub enum DataTransferDirection {
    NoDataTransfer,
    HostToController,
    ControllerToHost,
    Bidirectional,
}

pub trait Opcode {
    fn data_direction(self: &Self) -> DataTransferDirection;
}

impl<T> Opcode for T
where
    T: Copy,
    u8: From<T>,
{
    fn data_direction(self: &Self) -> DataTransferDirection {
        match u8::from(*self) & 0x03 {
            0b01 => DataTransferDirection::HostToController,
            0b10 => DataTransferDirection::ControllerToHost,
            0b11 => DataTransferDirection::Bidirectional,
            _ => DataTransferDirection::NoDataTransfer,
        }
    }
}

impl From<SpecAdminOpcode> for AdminOpcode {
    fn from(value: SpecAdminOpcode) -> Self {
        AdminOpcode::SpecOpcode(value)
    }
}

impl TryFrom<u8> for SpecAdminOpcode {
    type Error = UnrecognizedOpcode;

    /// Attempt to convert an u8 into a specification compliant operation code. Err means it is
    /// either a vendor specific code, or it is reserved.
    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x03
            | 0x07
            | 0x0b
            | 0x0e..=0x0f
            | 0x12..=0x13
            | 0x016..=0x17
            | 0x1b
            | 0x1f..=0x7b
            | 0x7d..=0x7e
            | 0x83
            | 0x85
            | 0x87..=0xbf => Err(UnrecognizedOpcode::Reserved(value)),
            0xc0..=0xff => Err(UnrecognizedOpcode::VendorSpecific(value)),
            _ => Ok(unsafe { transmute(value) }),
        }
    }
}

impl From<SpecAdminOpcode> for u8 {
    fn from(value: SpecAdminOpcode) -> Self {
        unsafe { transmute(value) }
    }
}

impl From<SpecNVMOpcode> for NVMOpcode {
    fn from(value: SpecNVMOpcode) -> Self {
        NVMOpcode::SpecOpcode(value)
    }
}

impl TryFrom<u8> for SpecNVMOpcode {
    type Error = UnrecognizedOpcode;

    /// Attempt to convert an u8 into a specification compliant operation code. Err means it is
    /// either a vendor specific code, or it is reserved.
    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x03 | 0x06..=0x07 | 0x0a..=0x0b | 0x0f..=0x10 | 0x12..=0x14 | 0x16..=0x7f => {
                Err(UnrecognizedOpcode::Reserved(value))
            }
            0x80..=0xff => Err(UnrecognizedOpcode::VendorSpecific(value)),
            _ => Ok(unsafe { transmute(value) }),
        }
    }
}

impl From<SpecNVMOpcode> for u8 {
    fn from(value: SpecNVMOpcode) -> Self {
        unsafe { transmute(value) }
    }
}

/// An enumeration of all admin operation codes described in the NVM Specification.
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
#[repr(u8)]
pub enum SpecAdminOpcode {
    DeleteIOSubmissionQueue = 0x00,
    CreateIOSubmissionQueue = 0x01,
    GetLogPage = 0x02,
    DeleteIOCompletionQueue = 0x04,
    CreateIOCompletionQueue = 0x05,
    Identify = 0x06,
    Abort = 0x08,
    SetFeatures = 0x09,
    GetFeatures = 0x0a,
    AsynchronousEventRequest = 0x0c,
    NamespaceManagement = 0x0d,
    FirmwareCommit = 0x10,
    FirmwareImageDownload = 0x11,
    DeviceSelfTest = 0x14,
    NamespaceAttachment = 0x15,
    KeepAlive = 0x18,
    DirectiveSend = 0x19,
    DirectiveReceive = 0x1a,
    VirtualizationManagement = 0x1c,
    NVMeMISend = 0x1d,
    NVMeMIReceive = 0x1e,
    DoorbellBufferConfig = 0x7c,
    Fabrics = 0x7f,
    FormatNVM = 0x80,
    SecuritySend = 0x81,
    SecurityReceive = 0x82,
    Sanitize = 0x84,
    GetLBAStatus = 0x86,
}

/// An enumeration of all NVM operation codes described in the NVM Specification.
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
#[repr(u8)]
pub enum SpecNVMOpcode {
    Flush = 0x00,
    Write = 0x01,
    Read = 0x02,
    WriteUncorrectable = 0x04,
    Compare = 0x05,
    WriteZeroes = 0x08,
    DatasetManagement = 0x09,
    Verify = 0x0c,
    ReservationRegister = 0x0d,
    ReservationReport = 0x0e,
    ReservationAcquire = 0x11,
    ReservationRelease = 0x15,
}
