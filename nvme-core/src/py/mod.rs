/*
 * Copyright (C) 2019  Oakes, Gregory <gregory.oakes@outlook.com>
 * Author: Oakes, Gregory <gregory.oakes@outlook.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

use std::time::Duration;

use pyo3::{exceptions::PyIOError, prelude::*, wrap_pyfunction};

use crate::{models::*, opcode::AdminOpcode, CommandBuilder, Device, Status};

#[pyclass]
struct CmdResult {
    #[pyo3(get)]
    pub status: Status,
    #[pyo3(get)]
    pub bytes: Option<Vec<u8>>,
}

#[pyfunction]
fn admin_passthru(
    device: String,
    opcode: u8,
    flags: Option<u8>,
    nsid: Option<u32>,
    data_len: Option<usize>,
    cdw10: Option<u32>,
    cdw11: Option<u32>,
    cdw12: Option<u32>,
    cdw13: Option<u32>,
    cdw14: Option<u32>,
    cdw15: Option<u32>,
    timeout: Option<u32>,
) -> PyResult<CmdResult> {
    let mut cmd = CommandBuilder::new(AdminOpcode::from(opcode));
    flags.map(|f| cmd.flags(f));
    nsid.map(|n| cmd.nsid(n));
    data_len.map(|d| cmd.data_len(d));
    cdw10.map(|w| cmd.cdw10(w));
    cdw11.map(|w| cmd.cdw11(w));
    cdw12.map(|w| cmd.cdw12(w));
    cdw13.map(|w| cmd.cdw13(w));
    cdw14.map(|w| cmd.cdw14(w));
    cdw15.map(|w| cmd.cdw15(w));
    timeout.map(|w| cmd.timeout(Duration::from_millis(w.into())));
    let mut dev = Device::open(device).map_err(|e| PyIOError::new_err(e.to_string()))?;
    let (status, bytes) = cmd
        .submit(&mut dev)
        .map_err(|e| PyIOError::new_err(e.to_string()))?;
    Ok(CmdResult {
        status,
        bytes: bytes.map(Vec::from),
    })
}

#[pyfunction]
fn id_ctrl(device: String) -> PyResult<PyObject> {
    let mut dev = Device::open(device).map_err(|e| PyIOError::new_err(e.to_string()))?;
    match dev.query::<IDCtrl>() {
        Ok(id_ctrl) => Ok(id_ctrl.as_ref().into_py(Python::acquire_gil().python())),
        Err(e) => Err(PyIOError::new_err(e.to_string())),
    }
}

#[pymodule]
pub fn nvme_core(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<IDCtrl>()?;
    m.add_class::<IDNamespace>()?;
    m.add_class::<logpage::FwSlotLog>()?;
    m.add_class::<logpage::ErrorLogEntry>()?;
    m.add_class::<logpage::SmartLog>()?;
    m.add_function(wrap_pyfunction!(admin_passthru, m)?)?;
    m.add_function(wrap_pyfunction!(id_ctrl, m)?)?;
    Ok(())
}
