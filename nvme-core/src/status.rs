/*
 * Copyright (C) 2019  Oakes, Gregory <gregory.oakes@outlook.com>
 * Author: Oakes, Gregory <gregory.oakes@outlook.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

//! A collection of structs for interpretting raw NVM Command results as meaningful Rust enums.

use std::{
    convert::{From, TryFrom, TryInto},
    fmt,
    mem::transmute,
};

#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub enum Status {
    LinuxStatus {
        /// If true, further issuance of the same command to the same controller is expected to fail.
        do_not_retry: bool,

        /// If true, there is additional information regarding the failure in the error log (log page
        /// 0x01).
        more_in_error_log: bool,

        /// If None, the command may be retried immediately. If Some, the returned value is an index in
        /// the identify controller struct's CRDT field which will specify the amount of time which
        /// must be waited before the command may be retried.
        cmd_retry_delay_idx: Option<u8>,
        status_code: StatusCode,
        raw_code: u16,
    },
    WindowsStatus {
        raw_code: i32,
    },
}

#[cfg(feature = "python")]
impl pyo3::IntoPy<pyo3::PyObject> for Status {
    fn into_py(self, py: pyo3::Python) -> pyo3::PyObject {
        self.raw_code().into_py(py)
    }
}

impl fmt::Display for Status {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Status::LinuxStatus {
                status_code,
                more_in_error_log: true,
                ..
            } => write!(
                f,
                "{} There is more info available in the error log page.",
                status_code
            ),
            Status::LinuxStatus { status_code, .. } => write!(f, "{}", status_code),
            _ => write!(f, "unknown status"),
        }
    }
}

impl Status {
    /// Parse the status field as per the Completion Queue Entry: Status Field specification.
    pub fn from_linux(value: u16) -> Self {
        let crd = ((value & 0x1800) >> 11) as u8;
        Status::LinuxStatus {
            do_not_retry: value & 0x4000 != 0,
            more_in_error_log: value & 0x2000 != 0,
            cmd_retry_delay_idx: if crd == 0 { None } else { Some(crd) },
            status_code: StatusCode::from(value & 0x7ff),
            raw_code: value,
        }
    }

    pub fn from_windows(value: i32) -> Self {
        Status::WindowsStatus { raw_code: value }
    }

    pub fn raw_code(self: &Self) -> i32 {
        match self {
            Status::LinuxStatus { raw_code, .. } => *raw_code as i32,
            Status::WindowsStatus { raw_code, .. } => *raw_code,
        }
    }

    /// If true, the command completed successfully with no further operations required. This is
    /// merely sugar simplifying long matches.
    pub fn was_successful(self: Self) -> bool {
        match self {
            Status::LinuxStatus {
                status_code: StatusCode::SpecStatusCode(SpecStatusCode::Successful),
                ..
            } => true,
            Status::WindowsStatus { raw_code, .. } if raw_code != 0 => true,
            _ => false,
        }
    }
}

/// Abstraction on top of NVM Specification status codes which allows for distinction between base
/// specification statuses, vendor specific statuses, and theoretically never used reserved
/// statuses.
#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq, thiserror::Error)]
pub enum StatusCode {
    #[error(transparent)]
    SpecStatusCode(#[from] SpecStatusCode),
    #[error("Vendor specific status code: {0}")]
    VendorSpecific(u16),
    #[error("Reserved status code: {0}")]
    Reserved(u16),
}

impl From<u16> for StatusCode {
    /// Parse the status code into either a recognized, specification compliant status code or into
    /// an unrecognized vendor specific or theoretically impossible reserved code.
    fn from(value: u16) -> Self {
        match value.try_into() {
            Ok(spec_code) => StatusCode::SpecStatusCode(spec_code),
            Err(unrec) => StatusCode::from(unrec),
        }
    }
}

impl From<UnrecognizedStatusCode> for StatusCode {
    fn from(value: UnrecognizedStatusCode) -> Self {
        match value {
            UnrecognizedStatusCode::VendorSpecific(code) => StatusCode::VendorSpecific(code),
            UnrecognizedStatusCode::Reserved(code) => StatusCode::Reserved(code),
        }
    }
}

#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub enum UnrecognizedStatusCode {
    VendorSpecific(u16),
    Reserved(u16),
}

impl TryFrom<u16> for SpecStatusCode {
    type Error = UnrecognizedStatusCode;

    /// Categorize an input status code raw value into either a specification compliant, vendor
    /// specific, or theoretically impossible reserved status code.
    fn try_from(value: u16) -> Result<Self, Self::Error> {
        match value {
            0x017
            | 0x023..=0x07f
            | 0x085..=0x0bf
            | 0x104
            | 0x117
            | 0x126..=0x16f
            | 0x183..=0x1bf
            | 0x200..=0x27f
            | 0x288..=0x2bf
            | 0x304..=0x35f
            | 0x361..=0x36f
            | 0x372..=0x37f => Err(UnrecognizedStatusCode::Reserved(value)),
            0x0c0..=0x0ff | 0x1c0..=0x1ff | 0x2c0..=0x2ff | 0x3c0..=0x3ff => {
                Err(UnrecognizedStatusCode::VendorSpecific(value))
            }
            _ => Ok(unsafe { transmute(value) }),
        }
    }
}

/// An enumeration of all status codes described in the NVM Specification.
#[derive(displaydoc::Display, Debug, Copy, Clone, Hash, Eq, PartialEq, thiserror::Error)]
#[repr(u16)]
pub enum SpecStatusCode {
    // Generic Status Codes
    /// The command completed without error.
    Successful = 0x000,
    /// A reserved coded value or an unsupported value in the command opcode field.
    InvalidOpcode = 0x001,
    /// A reserved coded value or an unsupported in a defined field (other than the opcode field).
    /// This status code should be used unless another status code is explicitly specified for a
    /// particular condition. The field may be in the command parameters as part of the Submission
    /// Queue Entry or in data structures pointed to by the command paramters.
    InvalidField = 0x002,
    /// Command ID Conflict: The command identifier is already in use. Note: It is implementation
    /// specific how many commands are searched for a conflict.
    IDConflict = 0x003,
    /// Transferring the data or metadata associated with a command had an error.
    DataTransferError = 0x004,
    /// Indicates that the command was aborted due to a power loss notification.
    AbortPowerLoss = 0x005,
    /// The command was not completed successfully due to an internal error. Details on the
    /// internal device error should be reported as an asynchronous event.
    InternalError = 0x006,
    /// The command was aborted due to an Abort command being received that specified the
    /// Submission Queue Identifier and Command Identifier of this command.
    AbortRequested = 0x007,
    /// The command was aborted due to a Delete I/O Submission Queue request received for the
    /// Submission Queue to which the command was submitted.
    AbortSQDelete = 0x008,
    /// The command was aborted due to the other command in a fused operation failing.
    AbortFailedFusedCommand = 0x009,
    /// The fused command was aborted due to the adjacent submission queue entry not containing a
    /// fused command that is the other command in a supported fused operation.
    AbortMissingFusedCommand = 0x00a,
    /// The namespace or the format of that namespace is invalid.
    InvalidNamespaceOrFormat = 0x00b,
    /// The command was aborted due to a protocol violation in a multi-command sequence (e.g., a
    /// violation of the Security Send and Security Receive sequencing rules in the TCG Storage
    /// Synchronous Interface Communications protocol (refer to TCG Storage Architecture Core
    /// Specification)).
    CommandSequenceError = 0x00c,
    /// The command includes an invalid SGL Last Segment or SGL Segment descriptor. This may occur
    /// when the SGL segment pointed to by an SGL Last Segment descriptor contains an SGL Segment
    /// descriptor or an SGL Last Segment descriptor or an SGL Segment descriptor. This may occur
    /// when an SGL Last Segment descriptor contains an invalid length (i.e., a length of 0h or 1h
    /// that is not a multiple of 16).
    InvalidSGLSegmentDescriptor = 0x00d,
    /// There is an SGL Last Segment descriptor or an SGL Segment descriptor in a location other
    /// than the last descriptor of a segment based on the length indicated.
    InvalidNumberOfSGLDescriptors = 0x00e,
    /// This may occur if the length of a Data SGL is too short. This may occur if the length of a
    /// Data SGL is too long and the controller does not support SGL transfers longer than the
    /// amount of data to be transferred as indicated in the SGL Support field of the Identify
    /// Controller data structure.
    DataSGLLengthInvalid = 0x00f,
    /// This may occur if the length of a Metadata SGL is too short. This may occur if the length
    /// of a Metadata SGL is too long and the controller does not support SGL transfers longer than
    /// the amount of data to be transferred as indicated in the SGL Support field of the Identify
    /// Controller data structure.
    MetadataSGLLengthInvalid = 0x010,
    /// The type of an SGL Descriptor is a type that is not supported by the
    SGLDescriptorTypeInvalid = 0x011,
    /// The attempted use of the Controller Memory Buffer is not supported by the controller.
    InvalidUseOfControllerMemoryBuffer = 0x012,
    /// The Offset field for a PRP entry is invalid. This may occur when there is a PRP entry with
    /// a non-zero offset after the first entry or when the Offset field in any PRP entry is not
    /// dword
    PRPOffsetInvalid = 0x013,
    /// The length specified exceeds the atomic write unit size.
    AtomicWriteUnitExceeded = 0x014,
    /// The command was denied due to lack of access rights. Refer to the appropriate security
    /// specification (e.g., TCG Storage Interface Interactions specification). For media access
    /// commands, the Access Denied status code should be used instead.
    OperationDenied = 0x015,
    /// The offset specified in a descriptor is invalid. This may occur when using capsules for
    /// data transfers in NVMe over Fabrics implementations and an invalid offset in the capsule is
    /// specified.
    SGLOffsetInvalid = 0x016,
    /// The NVM subsystem detected the simultaneous use of 64-bit and 128-bit Host Identifier
    /// values on different controllers.
    HostIdentifierInconsistentFormat = 0x018,
    /// The Keep Alive Timer expired.
    KeepAliveTimerExpired = 0x019,
    /// The Keep Alive Timeout value specified is invalid. This may be due to an attempt to specify
    /// a value of 0h on a transport that requires the Keep Alive feature to be enabled. This may
    /// be due to the value specified being too large for the associated NVMe Transport as defined
    /// in the NVMe Transport binding specification.
    KeepAliveTimeoutInvalid = 0x01a,
    /// The command was aborted due to a Reservation Acquire command with the Reservation Acquire
    /// Action (RACQA) set to 010b (Preempt and Abort).
    AbortPrempted = 0x01b,
    /// The most recent sanitize operation failed and no recovery action has been successfully
    /// completed.
    SanitizeFailed = 0x01c,
    /// The requested function (e.g., command) is prohibited while a sanitize operation is in
    /// progress.
    SanitizeInProgress = 0x01d,
    /// The Address alignment or Length granularity for an SGL Data Block descriptor is invalid.
    /// This may occur when a controller supports dword granularity only and the lower two bits of
    /// the Address or Length are not cleared to 00b.
    SGLDataBlockGranularityInvalid = 0x01e,
    /// The implementation does not support submission of the command to a Submission Queue in the
    /// Controller Memory Buffer or command completion to a Completion Queue in the Controller
    /// Memory Buffer.
    NotSupportedForQueueInCMB = 0x01f,
    /// The command is prohibited while the namespace is write protected as a result of a change in
    /// the namespace write protection state as defined by the Namespace Write Protection State
    /// Machine.
    NamespaceIsWriteProtected = 0x020,
    /// Command processing was interrupted and the controller is unable to successfully complete
    /// the command. The host should retry the command.  If this status code is returned, then the
    /// controller shall clear the Do Not Retry bit to ‘0’ in the Status field of the CQE. The
    /// controller shall not return this status code unless the host has set the Advanced Command
    /// Retry Enable (ACRE) field to 1h in the Host Behavior Support feature.
    CommandInterrupted = 0x021,
    /// A transient transport error was detected. If the command is retried on the same controller,
    /// the command is likely to succeed. A command that fails with a transient transport error
    /// four or more times should be treated as a persistent transport error that is not likely
    TransientTransportError = 0x022,

    /// The command references an LBA that exceeds the size of the namespace.
    LBAOutOfRange = 0x080,
    /// Execution of the command has caused the capacity of the namespace to be exceeded. This
    /// error occurs when the Namespace Utilization exceeds the Namespace Capacity.
    CapacityExceeded = 0x081,
    /// The namespace is not ready to be accessed as a result of a condition other than a condition
    /// that is reported as an Asymmetric Namespace Access condition. The Do Not Retry bit
    /// indicates whether re-issuing the command at a later time may succeed.
    NamespaceNotReady = 0x082,
    /// The command was aborted due to a conflict with a reservation held on the accessed
    /// namespace.
    ReservationConflict = 0x083,
    /// A Format NVM command is in progress on the namespace. The Do Not Retry bit shall be cleared
    /// to '0' to indicate that the command may succeed if resubmitted.
    FormatInProgress = 0x084,

    // Command Specific Status Codes
    /// Completion queue invalid.
    CompletionQueueInvalid = 0x100,
    /// Invalid queue identifier.
    InvalidQueueIdentifier = 0x101,
    /// Invalid queue size.
    InvalidQueueSize = 0x102,
    /// Abort command limit exceeded.
    AbortCommandLimitExceeded = 0x103,

    /// Abort command limit exceeded.
    AsynchronousEventRequestLimitExceeded = 0x105,
    /// Invalid firmware slot.
    InvalidFirmwareSlot = 0x106,
    /// Invalid firmware image.
    InvalidFirmwareImage = 0x107,
    /// Invalid interrupt vector.
    InvalidInterruptVector = 0x108,
    /// Invalid log page.
    InvalidLogPage = 0x109,
    /// Invalid format.
    InvalidFormat = 0x10a,
    /// Firmware activation requires conventional reset.
    FirmwareActivationRequiresConventionalReset = 0x10b,
    /// Invalid queue deletion.
    InvalidQueueDeletion = 0x10c,
    /// Feature identifier not saveable.
    FeatureIdentifierNotSaveable = 0x10d,
    /// Feature not changeable.
    FeatureNotChangeable = 0x10e,
    /// Feature not namespace specific.
    FeatureNotNamespaceSpecific = 0x10f,
    /// Firmware activation requires NVM subsystem reset.
    FirmwareActivationRequiresNVMSubsystemReset = 0x110,
    /// Firmware activation requires controller level reset.
    FirmwareActivationRequiresControllerLevelReset = 0x111,
    /// Firmware activation requires maximum time violation.
    FirmwareActivationRequiresMaximumTimeViolation = 0x112,
    /// Firmware activation prohibited.
    FirmwareActivationProhibited = 0x113,
    /// Overlapping range.
    OverlappingRange = 0x114,
    /// Namespace insufficient capacity.
    NamespaceInsufficientCapacity = 0x115,
    /// Namespace identifier unavailable.
    NamespaceIdentifierUnavailable = 0x116,

    /// Namespace alredy attached.
    NamespaceAlreadyAttached = 0x118,
    /// Namespace is private.
    NamespaceIsPrivate = 0x119,
    /// Namespace not attached.
    NamespaceNotAttached = 0x11a,
    /// Thin provisioning not supported.
    ThinProvisioningNotSupported = 0x11b,
    /// Controller list invalid.
    ControllerListInvalid = 0x11c,
    /// Device self test in progress.
    DeviceSelfTestInProgress = 0x11d,
    /// Boot partition write prohibited.
    BootPartitionWriteProhibited = 0x11e,
    /// Invalid controller identifier.
    InvalidControllerIdentifier = 0x11f,
    /// Invalid secondary controller state.
    InvalidSecondaryControllerState = 0x120,
    /// Invalid number of controller resources.
    InvalidNumberofControllerResources = 0x121,
    /// Invalid resource identifier.
    InvalidResourceIdentifier = 0x122,
    /// Sanitize prohibited while persistent memory region is enabled.
    SanitizeProhibitedWhilePersistentMemoryRegionIsEnabled = 0x123,
    /// ANA group identifier invalid.
    ANAGroupIdentifierInvalid = 0x124,
    /// ANA attach failed.
    ANAAttachFailed = 0x125,

    // TODO: Directive Specific Statuses
    /// Conflicting attributes.
    ConflictingAttributes = 0x180,
    /// Invalid protection information.
    InvalidProtectionInformation = 0x181,
    /// Attempted write to read only range.
    AttemptedWriteToReadOnlyRange = 0x182,

    // Media and Data Inegrity Errors
    /// The write data could not be committed to the media.
    WriteFault = 0x280,
    /// The read data could not be recovered from the media.
    UnrecoveredReadError = 0x281,
    /// The command was aborted due to an end-to-end guard check failure.
    EndToEndGuardCheckError = 0x282,
    /// The command was aborted due to an end-to-end application tag check failure.
    EndToEndApplicationTagCheckError = 0x283,
    /// The command was aborted due to an end-to-end reference tag check failure.
    EndToEndReferenceTagCheckError = 0x284,
    /// The command failed due to a miscompare during a Compare command.
    CompareFailure = 0x285,
    /// Access to the namespace and/or LBA range is denied due to lack of access rights.
    AccessDenied = 0x286,
    /// The commmand failed due to an attempt to read from or verify an LBA range containing a
    /// deallocated or unwritten logical block.
    DeallocatedOrUnwrittenLogicalBlock = 0x287,

    // Path Related Status Codes
    /// The command was not completed as the result of a controller internal error that is specific
    /// to the controller processing the command. Retries for the request function should be based
    /// on the setting of the DNR bit.
    InternalPathError = 0x300,
    /// The requested function (e.g., command) is not able to be performed as a result of the
    /// relationship between the controller and the namespace being in the ANA Persistent Loss
    /// state. The command should not be re-submitted to the same controller.
    AsymmetricAccessPersistentLoss = 0x301,
    /// The requested function (e.g., command) is not able to be performed as a result of the
    /// relationship between the controller and the namespace being in the ANA Inaccessible state.
    /// The command should not be re-submitted to the same controller.
    AsymmetricAccessInaccessible = 0x302,
    /// The requested function (e.g., command) is not able to be performed as a result of the
    /// relationship between the controller and the namespace transitioning between Asymmetric
    /// Namespace Access states. The requested function
    AsymmetricAccessTransition = 0x303,
    /// A pathing error was detected by the controller.
    ControllerPathingError = 0x360,
    /// A pathing error was detected by the host.
    HostPathingError = 0x370,
    /// The command was aborted as a result of host action (e.g., the host disconnected the Fabric
    /// connection).
    CommandAbortedByHost = 0x371,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn status_from_u16() {
        if let Status::LinuxStatus {
            status_code,
            do_not_retry,
            more_in_error_log,
            cmd_retry_delay_idx,
            ..
        } = Status::from_linux(0x0000)
        {
            assert_eq!(
                status_code,
                StatusCode::SpecStatusCode(SpecStatusCode::Successful)
            );
            assert!(!do_not_retry);
            assert!(!more_in_error_log);
            assert_eq!(cmd_retry_delay_idx, None);
        }

        if let Status::LinuxStatus {
            status_code,
            do_not_retry,
            more_in_error_log,
            cmd_retry_delay_idx,
            ..
        } = Status::from_linux(0x0371)
        {
            assert_eq!(
                status_code,
                StatusCode::SpecStatusCode(SpecStatusCode::CommandAbortedByHost)
            );
            assert!(!do_not_retry);
            assert!(!more_in_error_log);
            assert_eq!(cmd_retry_delay_idx, None);
        }

        if let Status::LinuxStatus {
            status_code,
            do_not_retry,
            more_in_error_log,
            cmd_retry_delay_idx,
            ..
        } = Status::from_linux((1 << 14) | (1 << 13) | (2 << 11) | 0x0001)
        {
            assert_eq!(
                status_code,
                StatusCode::SpecStatusCode(SpecStatusCode::InvalidOpcode)
            );
            assert!(do_not_retry);
            assert!(more_in_error_log);
            assert_eq!(cmd_retry_delay_idx, Some(2));
        }

        if let Status::LinuxStatus {
            status_code,
            do_not_retry,
            more_in_error_log,
            cmd_retry_delay_idx,
            ..
        } = Status::from_linux(0x00ff)
        {
            assert_eq!(status_code, StatusCode::VendorSpecific(0x00ff));
            assert!(!do_not_retry);
            assert!(!more_in_error_log);
            assert_eq!(cmd_retry_delay_idx, None);
        }
    }
}
