/*
 * Copyright (C) 2020  Oakes, Gregory <gregoryoakes@fastmail.com>
 * Author: Oakes, Gregory <gregory.oakes@fastmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

use std::{fs::File, mem, path::Path};

use crate::{models::*, CommandBuilder, Error, LogPageIter, Status};

#[derive(Debug)]
pub struct Device {
    pub fd: File,
}

impl Device {
    pub fn open<P: AsRef<Path>>(path: P) -> Result<Device, Error> {
        Ok(Device {
            fd: File::open(path)?,
        })
    }

    pub fn query_status<T: FromDevice>(&mut self) -> Result<(Status, Box<T>), Error> {
        T::from_device_status(self)
    }

    pub fn query_raw_status<T: FromDevice>(&mut self) -> Result<(Status, Box<[u8]>), Error> {
        T::from_device_raw_status(self)
    }

    pub fn query_ns_status<T: FromNamespace>(
        &mut self,
        nsid: u32,
    ) -> Result<(Status, Box<T>), Error> {
        T::from_namespace_status(self, nsid)
    }

    pub fn query_ns_raw_status<T: FromNamespace>(
        &mut self,
        nsid: u32,
    ) -> Result<(Status, Box<[u8]>), Error> {
        T::from_namespace_raw_status(self, nsid)
    }

    pub fn query<T: FromDevice>(&mut self) -> Result<Box<T>, Error> {
        T::from_device(self)
    }

    pub fn query_raw<T: FromDevice>(&mut self) -> Result<Box<[u8]>, Error> {
        T::from_device_raw(self)
    }

    pub fn query_ns<T: FromNamespace>(&mut self, nsid: u32) -> Result<Box<T>, Error> {
        T::from_namespace(self, nsid)
    }

    pub fn query_ns_raw<T: FromNamespace>(&mut self, nsid: u32) -> Result<Box<[u8]>, Error> {
        T::from_namespace_raw(self, nsid)
    }

    pub fn query_iter<'a, T, I>(&'a mut self, offset: usize, max: usize) -> I
    where
        T: FromDeviceIter<'a, Iterator = I>,
        I: Iterator<Item = T>,
    {
        T::from_device_iter(self, offset, max)
    }
}

pub trait FromDevice: Sized {
    fn from_device(dev: &mut Device) -> Result<Box<Self>, Error>;
    fn from_device_raw(dev: &mut Device) -> Result<Box<[u8]>, Error>;
    fn from_device_status(dev: &mut Device) -> Result<(Status, Box<Self>), Error>;
    fn from_device_raw_status(dev: &mut Device) -> Result<(Status, Box<[u8]>), Error>;
}

pub trait FromNamespace: Sized {
    fn from_namespace(dev: &mut Device, nsid: u32) -> Result<Box<Self>, Error>;
    fn from_namespace_raw(dev: &mut Device, nsid: u32) -> Result<Box<[u8]>, Error>;
    fn from_namespace_status(dev: &mut Device, nsid: u32) -> Result<(Status, Box<Self>), Error>;
    fn from_namespace_raw_status(dev: &mut Device, nsid: u32)
        -> Result<(Status, Box<[u8]>), Error>;
}

pub trait FromDeviceIter<'a>: Sized {
    type Iterator;
    fn from_device_iter(dev: &'a mut Device, offset: usize, max: usize) -> Self::Iterator;
}

macro_rules! impl_from_device {
    ( $type:ty, $cmd_builder:expr ) => {
        impl FromDevice for $type {
            fn from_device_raw_status(dev: &mut Device) -> Result<(Status, Box<[u8]>), Error> {
                let cmd = $cmd_builder;
                match cmd.submit(dev)? {
                    (status, Some(data)) if status.was_successful() => Ok((status, data)),
                    (status, _) => Err(Error::NVMStatus(status)),
                }
            }

            fn from_device_status(dev: &mut Device) -> Result<(Status, Box<Self>), Error> {
                let (s, d) = Self::from_device_raw_status(dev)?;
                Ok((s, Self::try_from_buf(d)?))
            }

            fn from_device_raw(dev: &mut Device) -> Result<Box<[u8]>, Error> {
                let (_, d) = Self::from_device_raw_status(dev)?;
                Ok(d)
            }

            fn from_device(dev: &mut Device) -> Result<Box<Self>, Error> {
                let (_, d) = Self::from_device_status(dev)?;
                Ok(d)
            }
        }
    };
}
macro_rules! impl_from_namespace {
    ( $type:ty, $cmd_builder:expr ) => {
        impl FromNamespace for $type {
            fn from_namespace_raw_status(
                dev: &mut Device,
                nsid: u32,
            ) -> Result<(Status, Box<[u8]>), Error> {
                let cmd = $cmd_builder.with_nsid(nsid);
                match cmd.submit(dev)? {
                    (status, Some(data)) if status.was_successful() => Ok((status, data)),
                    (status, _) => Err(Error::NVMStatus(status)),
                }
            }

            fn from_namespace_status(
                dev: &mut Device,
                nsid: u32,
            ) -> Result<(Status, Box<Self>), Error> {
                let (s, d) = Self::from_namespace_raw_status(dev, nsid)?;
                Ok((s, Self::try_from_buf(d)?))
            }

            fn from_namespace_raw(dev: &mut Device, nsid: u32) -> Result<Box<[u8]>, Error> {
                let (_, d) = Self::from_namespace_raw_status(dev, nsid)?;
                Ok(d)
            }

            fn from_namespace(dev: &mut Device, nsid: u32) -> Result<Box<Self>, Error> {
                let (_, d) = Self::from_namespace_status(dev, nsid)?;
                Ok(d)
            }
        }
    };
}

macro_rules! impl_log_from_device {
    ( $type:ty, $lid:expr ) => {
        impl_from_device!(
            $type,
            CommandBuilder::get_log_page($lid, 0, 0, 0, false, 0, mem::size_of::<Self>())
        );
    };
}

impl_from_device!(
    IDCtrl,
    CommandBuilder::identify(0, 1, 0, 0, mem::size_of::<Self>())
);
impl_from_namespace!(
    IDNamespace,
    CommandBuilder::identify(0, 0, 0, 0, mem::size_of::<Self>())
);
impl_log_from_device!(logpage::SmartLog, 0x02);
impl_log_from_device!(logpage::FwSlotLog, 0x03);
impl_log_from_device!(logpage::CmdEffectLog, 0x05);

pub struct ErrorLogIterator<'a> {
    page_iter: LogPageIter<'a>,
    page: Option<Box<[logpage::ErrorLogEntry]>>,
    err: usize,
}

impl<'a> Iterator for ErrorLogIterator<'a> {
    type Item = logpage::ErrorLogEntry;

    fn next(&mut self) -> Option<Self::Item> {
        match &self.page {
            Some(page) if self.err < page.len() => {
                let ret = page[self.err];
                self.err += 1;
                Some(ret)
            }
            _ => match self.page_iter.next() {
                Some(Ok((status, Some(data)))) if status.was_successful() => {
                    match <[logpage::ErrorLogEntry]>::try_from_buf(data) {
                        Ok(new_page) => {
                            self.page = Some(new_page);
                            self.err = 0;
                            self.next()
                        }
                        _ => None,
                    }
                }
                _ => None,
            },
        }
    }
}

impl<'a> FromDeviceIter<'a> for logpage::ErrorLogEntry {
    type Iterator = ErrorLogIterator<'a>;
    fn from_device_iter(dev: &'a mut Device, offset: usize, max: usize) -> Self::Iterator {
        ErrorLogIterator {
            page_iter: CommandBuilder::get_log_page(
                0x01,
                0,
                (offset * mem::size_of::<logpage::ErrorLogEntry>()) as u64,
                0,
                false,
                0,
                max * mem::size_of::<logpage::ErrorLogEntry>(),
            )
            .submit_get_log_iter(dev),
            page: None,
            err: 0,
        }
    }
}
