/*
 * Copyright (C) 2019  Oakes, Gregory <gregory.oakes@outlook.com>
 * Author: Oakes, Gregory <gregory.oakes@outlook.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

use std::{borrow::Cow, mem};

use crate::Error;

mod id_ctrl;
pub use id_ctrl::IDCtrl;

mod id_ns;
pub use id_ns::IDNamespace;

mod power_state;
pub use power_state::PowerState;

pub mod logpage;

#[derive(displaydoc::Display, Debug, Copy, Clone, thiserror::Error)]
pub enum FromBufError {
    /// The buffer's size is too small for the type.
    BufTooSmall(usize),
    /// The buffer's size is too big for the type.
    BufTooBig(usize),
    /// The buffer's size ({buf_size}) does not align with a single element's size ({entry_size}).
    BufUnaligned { buf_size: usize, entry_size: usize },
}

pub trait TryFromBuf {
    fn try_from_buf(buf: Box<[u8]>) -> Result<Box<Self>, FromBufError>;
}

impl<T: Sized + Copy> TryFromBuf for T {
    fn try_from_buf(buf: Box<[u8]>) -> Result<Box<Self>, FromBufError> {
        let size = buf.len();
        let expected_size = mem::size_of::<Self>();
        if size == expected_size {
            Ok(unsafe { Box::from_raw(Box::into_raw(buf) as *mut Self) })
        } else if size < expected_size {
            Err(FromBufError::BufTooSmall(size))
        } else {
            Err(FromBufError::BufTooBig(size))
        }
    }
}

#[derive(Copy, Clone)]
pub struct FixedCStr<const N: usize>([u8; N]);

impl<const N: usize> FixedCStr<N> {
    #[inline(always)]
    pub fn as_str(self: &FixedCStr<N>) -> Cow<str> {
        String::from_utf8_lossy(&self.0[..])
    }
}

#[cfg(feature = "serde")]
impl<const N: usize> serde::Serialize for FixedCStr<N> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        serializer.serialize_str(self.as_str().as_ref())
    }
}

#[cfg(feature = "pyo3")]
impl<const N: usize> pyo3::ToPyObject for FixedCStr<N> {
    fn to_object(&self, py: pyo3::Python) -> pyo3::PyObject {
        self.as_str().as_ref().to_object(py)
    }
}

#[cfg(feature = "pyo3")]
impl<const N: usize> pyo3::IntoPy<pyo3::PyObject> for FixedCStr<N> {
    fn into_py(self, py: pyo3::Python) -> pyo3::PyObject {
        self.as_str().as_ref().into_py(py)
    }
}
