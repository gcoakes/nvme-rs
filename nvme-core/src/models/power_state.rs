/*
 * Copyright (C) 2019  Oakes, Gregory <gregory.oakes@outlook.com>
 * Author: Oakes, Gregory <gregory.oakes@outlook.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#[cfg(feature = "python")]
use pyo3::prelude::*;

#[cfg_attr(feature = "python", nvme_macro::pyclass_readonly_all)]
#[cfg_attr(feature = "serde", derive(serde::Serialize))]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PowerState {
    pub max_power: u16,
    #[cfg_attr(feature = "serde", serde(skip))]
    #[allow(dead_code)]
    reserved2: u8,
    pub flags: u8,
    pub entry_lat: u32,
    pub exit_lat: u32,
    pub read_tput: u8,
    pub read_lat: u8,
    pub write_tput: u8,
    pub write_lat: u8,
    pub idle_power: u16,
    pub idle_scale: u8,
    #[cfg_attr(feature = "serde", serde(skip))]
    #[allow(dead_code)]
    reserved20: u8,
    pub active_power: u16,
    pub active_work_scale: u8,
    #[cfg_attr(feature = "serde", serde(skip))]
    #[allow(dead_code)]
    reserved25: [u8; 9],
}

#[test]
fn test_structure() {
    assert_eq!(std::mem::size_of::<PowerState>(), 32);
}
