/*
 * Copyright (C) 2019  Oakes, Gregory <gregory.oakes@outlook.com>
 * Author: Oakes, Gregory <gregory.oakes@outlook.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

use std::borrow::Cow;

#[cfg(feature = "python")]
use pyo3::prelude::*;

use crate::models::{FixedCStr, PowerState};

#[cfg_attr(feature = "python", nvme_macro::pyclass_readonly_all("psds", "vs"))]
#[cfg_attr(feature = "serde", derive(serde::Serialize))]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct IDCtrl {
    pub vid: u16,
    pub ssvid: u16,
    pub sn: FixedCStr<20>,
    pub mn: FixedCStr<40>,
    pub fr: FixedCStr<8>,
    pub rab: u8,
    pub ieee: [u8; 3],
    pub cmic: u8,
    pub mdts: u8,
    pub cntlid: u16,
    pub ver: u32,
    pub rtd3r: u32,
    pub rtd3e: u32,
    pub oaes: u32,
    pub ctratt: u32,
    pub rrls: u16,
    #[cfg_attr(feature = "serde", serde(skip))]
    #[allow(dead_code)]
    reserved102: [u8; 9],
    pub cntrltype: u8,
    pub fguid: u128,
    pub crdt: [u16; 3],
    #[cfg_attr(feature = "serde", serde(skip))]
    #[allow(dead_code)]
    reserved134: [u8; 122],
    pub oacs: u16,
    pub acl: u8,
    pub aerl: u8,
    pub frmw: u8,
    pub lpa: u8,
    pub elpe: u8,
    pub npss: u8,
    pub avscc: u8,
    pub apsta: u8,
    pub wctemp: u16,
    pub cctemp: u16,
    pub mtfa: u16,
    pub hmpre: u32,
    pub hmmin: u32,
    pub tnvmcap: u128,
    pub unvmcap: u128,
    pub rpmbs: u32,
    pub edstt: u16,
    pub dsto: u8,
    pub fwug: u8,
    pub kas: u16,
    pub hctma: u16,
    pub mntmt: u16,
    pub mxtmt: u16,
    pub sanicap: u32,
    pub hmminds: u32,
    pub hmmaxd: u16,
    pub nsetidmax: u16,
    pub endgidmax: u16,
    pub anatt: u8,
    pub anacap: u8,
    pub anagrpmax: u32,
    pub nanagrpid: u32,
    pub pels: u32,
    #[cfg_attr(feature = "serde", serde(skip))]
    #[allow(dead_code)]
    reserved356: [u8; 156],
    pub sqes: u8,
    pub cqes: u8,
    pub maxcmd: u16,
    pub nn: u32,
    pub oncs: u16,
    pub fuses: u16,
    pub fna: u8,
    pub vwc: u8,
    pub awun: u16,
    pub awupf: u16,
    pub nvscc: u8,
    pub nwpc: u8,
    pub acwu: u16,
    #[cfg_attr(feature = "serde", serde(skip))]
    #[allow(dead_code)]
    reserved534: [u8; 2],
    pub sgls: u32,
    pub mnan: u32,
    #[cfg_attr(feature = "serde", serde(skip))]
    #[allow(dead_code)]
    reserved544: [u8; 224],
    pub subnqn: FixedCStr<256>,
    #[cfg_attr(feature = "serde", serde(skip))]
    #[allow(dead_code)]
    reserved1024: [u8; 1024],
    pub psds: [PowerState; 32],
    #[cfg_attr(feature = "serde", serde(skip))]
    pub vs: [u8; 1024],
}

impl IDCtrl {
    #[inline(always)]
    pub fn spec_version(self: &IDCtrl) -> (u16, u8) {
        (
            ((self.ver & 0xffff0000) >> 16) as u16,
            ((self.ver & 0xff00) >> 8) as u8,
        )
    }

    #[inline(always)]
    pub fn serial(self: &IDCtrl) -> Cow<str> {
        self.sn.as_str()
    }

    #[inline(always)]
    pub fn model(self: &IDCtrl) -> Cow<str> {
        self.mn.as_str()
    }

    #[inline(always)]
    pub fn firmware(self: &IDCtrl) -> Cow<str> {
        self.fr.as_str()
    }
}

#[test]
fn test_structure() {
    assert_eq!(std::mem::size_of::<IDCtrl>(), 4096);
}
