/*
 * Copyright (C) 2020  Oakes, Gregory <gregoryoakes@fastmail.com>
 * Author: Oakes, Gregory <gregory.oakes@fastmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

use crate::models::FixedCStr;

#[cfg(feature = "python")]
use pyo3::prelude::*;

#[cfg_attr(feature = "python", nvme_macro::pyclass_readonly_all())]
#[cfg_attr(feature = "serde", derive(serde::Serialize))]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct FwSlotLog {
    pub afi: u8,
    #[cfg_attr(feature = "serde", serde(skip))]
    #[allow(dead_code)]
    reserved1: [u8; 7],
    pub slots: [FixedCStr<8>; 7],
}

#[test]
fn test_structure() {
    assert_eq!(std::mem::size_of::<FwSlotLog>(), 64);
}
