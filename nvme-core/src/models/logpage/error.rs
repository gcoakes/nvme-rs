/*
 * Copyright (C) 2020  Oakes, Gregory <gregoryoakes@fastmail.com>
 * Author: Oakes, Gregory <gregory.oakes@fastmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

use std::mem;

#[cfg(feature = "python")]
use pyo3::prelude::*;

use crate::models::{FromBufError, TryFromBuf};

impl TryFromBuf for [ErrorLogEntry] {
    fn try_from_buf(buf: Box<[u8]>) -> Result<Box<Self>, FromBufError> {
        let size = buf.len();
        let entry_size = mem::size_of::<ErrorLogEntry>();
        if size % entry_size == 0 {
            let size = buf.len() / entry_size;
            Ok(
                unsafe {
                    Vec::from_raw_parts(Box::into_raw(buf) as *mut ErrorLogEntry, size, size)
                }
                .into_boxed_slice(),
            )
        } else {
            Err(FromBufError::BufUnaligned {
                buf_size: size,
                entry_size,
            })
        }
    }
}

#[cfg_attr(feature = "python", nvme_macro::pyclass_readonly_all())]
#[cfg_attr(feature = "serde", derive(serde::Serialize))]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct ErrorLogEntry {
    pub error_count: u64,
    pub sqid: u16,
    pub cmdid: u16,
    pub status_field: u16,
    pub parm_error_location: u16,
    pub lba: u64,
    pub nsid: u32,
    pub vs: u8,
    pub trtype: u8,
    #[cfg_attr(feature = "serde", serde(skip))]
    #[allow(dead_code)]
    reserved30: [u8; 2],
    pub cs: u64,
    #[cfg_attr(feature = "serde", serde(skip))]
    #[allow(dead_code)]
    reserved40: [u8; 24],
}

#[test]
fn test_structure() {
    assert_eq!(mem::size_of::<ErrorLogEntry>(), 64);
}
