/*
 * Copyright (C) 2020  Oakes, Gregory <gregoryoakes@fastmail.com>
 * Author: Oakes, Gregory <gregory.oakes@fastmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#[cfg(feature = "python")]
use pyo3::prelude::*;

#[cfg_attr(feature = "python", nvme_macro::pyclass_readonly_all())]
#[cfg_attr(feature = "serde", derive(serde::Serialize))]
#[derive(Copy, Clone)]
#[repr(packed)]
pub struct SmartLog {
    pub critical_warning: u8,
    pub temperature: u16,
    pub avail_spare: u8,
    pub spare_thresh: u8,
    pub percent_used: u8,
    pub endurance_grp_critical_warning_summary: u8,
    #[cfg_attr(feature = "serde", serde(skip))]
    #[allow(dead_code)]
    reserved7: [u8; 25],
    pub data_units_read: u128,
    pub data_units_written: u128,
    pub host_read_commands: u128,
    pub host_write_commands: u128,
    pub controller_busy_time: u128,
    pub power_cycles: u128,
    pub power_on_hours: u128,
    pub unsafe_shutdowns: u128,
    pub media_errors: u128,
    pub num_err_log_entries: u128,
    pub warning_temp_time: u32,
    pub critical_comp_time: u32,
    pub temp_sensors: [u16; 8],
    pub thm_temp1_transition_count: u32,
    pub thm_temp2_transition_count: u32,
    pub thm_temp1_total_time: u32,
    pub thm_temp2_total_time: u32,
    #[cfg_attr(feature = "serde", serde(skip))]
    #[allow(dead_code)]
    reserved232: [u8; 280],
}

#[test]
fn test_structure() {
    assert_eq!(std::mem::size_of::<SmartLog>(), 512);
}
