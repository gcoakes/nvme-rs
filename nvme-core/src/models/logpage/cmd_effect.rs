/*
 * Copyright (C) 2020  Oakes, Gregory <gregoryoakes@fastmail.com>
 * Author: Oakes, Gregory <gregory.oakes@fastmail.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#[cfg(feature = "serde")]
use std::ops::Index;

#[cfg(feature = "serde")]
use serde::ser::{SerializeMap, SerializeSeq};

#[cfg_attr(feature = "serde", derive(serde::Serialize))]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct CmdEffectLog {
    #[cfg_attr(feature = "serde", serde(serialize_with = "ser_eff"))]
    pub admin_effects: [CmdEffect; 256],
    #[cfg_attr(feature = "serde", serde(serialize_with = "ser_eff"))]
    pub io_effects: [CmdEffect; 256],
    #[cfg_attr(feature = "serde", serde(skip))]
    #[allow(dead_code)]
    reserved2048: [u8; 2048],
}

#[derive(Copy, Clone)]
#[repr(transparent)]
pub struct CmdEffect(u32);

#[cfg(feature = "serde")]
impl serde::Serialize for CmdEffect {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let mut map = serializer.serialize_map(Some(7))?;
        map.serialize_entry("supported", &self.supported())?;
        map.serialize_entry("changes_logical_block", &self.changes_logical_block())?;
        map.serialize_entry("changes_ns_capabilities", &self.changes_ns_capabilities())?;
        map.serialize_entry("changes_ns_inventory", &self.changes_ns_inventory())?;
        map.serialize_entry("changes_ctrl_inventory", &self.changes_ctrl_capabilities())?;
        map.serialize_entry("uuid_selection_supported", &self.uuid_selection_supported())?;
        map.end()
    }
}

#[derive(Copy, Clone)]
pub enum CmdSubRestriction {
    Unrestricted,
    SameNamespace,
    AnyNamespace,
}

impl CmdEffect {
    pub fn supported(&self) -> bool {
        self.0 & 1 == 1
    }

    pub fn changes_logical_block(&self) -> bool {
        (self.0 & (1 << 1)) > 0
    }

    pub fn changes_ns_capabilities(&self) -> bool {
        self.0 & (1 << 2) > 0
    }

    pub fn changes_ns_inventory(&self) -> bool {
        self.0 & (1 << 3) > 0
    }

    pub fn changes_ctrl_capabilities(&self) -> bool {
        self.0 & (1 << 4) > 0
    }

    pub fn sub_restrictions(&self) -> CmdSubRestriction {
        match self.0 & (7 << 16) {
            0 => CmdSubRestriction::Unrestricted,
            1 => CmdSubRestriction::SameNamespace,
            2 => CmdSubRestriction::AnyNamespace,
            _ => panic!("Unknown command submission restriction."),
        }
    }

    pub fn uuid_selection_supported(&self) -> bool {
        self.0 & (1 << 19) > 0
    }
}

#[cfg(feature = "serde")]
fn ser_eff<S>(item: &[CmdEffect; 256], serializer: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    let mut seq = serializer.serialize_seq(Some(item.len()))?;
    for opcode in 0..item.len() {
        seq.serialize_element(item.index(opcode))?;
    }
    seq.end()
}

#[test]
fn test_structure() {
    assert_eq!(std::mem::size_of::<CmdEffectLog>(), 4096);
}
