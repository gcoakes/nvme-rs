/*
 * Copyright (C) 2019  Oakes, Gregory <gregory.oakes@outlook.com>
 * Author: Oakes, Gregory <gregory.oakes@outlook.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#[cfg(feature = "python")]
use pyo3::prelude::*;

#[cfg_attr(feature = "python", nvme_macro::pyclass_readonly_all("lbafs", "vs"))]
#[cfg_attr(feature = "serde", derive(serde::Serialize))]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct IDNamespace {
    pub nsze: u64,
    pub ncap: u64,
    pub nuse: u64,
    pub nsfeat: u8,
    pub nlbaf: u8,
    pub flbas: u8,
    pub mc: u8,
    pub dpc: u8,
    pub dps: u8,
    pub nmic: u8,
    pub rescap: u8,
    pub fpi: u8,
    pub dlfeat: u8,
    pub nawun: u16,
    pub nawupf: u16,
    pub nacwu: u16,
    pub nabsn: u16,
    pub nabo: u16,
    pub nabspf: u16,
    pub noiob: u16,
    pub nvmcap: u128,
    pub npwg: u16,
    pub npwa: u16,
    pub npdg: u16,
    pub npda: u16,
    pub nows: u16,
    #[cfg_attr(feature = "serde", serde(skip))]
    reserved74: [u8; 18],
    pub anagrpid: u32,
    #[cfg_attr(feature = "serde", serde(skip))]
    reserved96: [u8; 3],
    pub nsattr: u8,
    pub nvmsetid: u16,
    pub endgid: u16,
    pub nguid: u128,
    pub eui64: u64,
    pub lbafs: [LBAFormat; 16],
    #[cfg_attr(feature = "serde", serde(skip))]
    reserved192: [u8; 192],
    #[cfg_attr(feature = "serde", serde(skip))]
    pub vs: [u8; 3712],
}

#[cfg_attr(feature = "serde", derive(serde::Serialize))]
#[derive(Copy, Clone)]
#[repr(C)]
pub struct LBAFormat {
    pub ms: u16,
    pub lbads: u8,
    pub rp: u8,
}

#[test]
fn test_structure() {
    assert_eq!(std::mem::size_of::<IDNamespace>(), 4096);
}
