/*
 * Copyright (C) 2019  Oakes, Gregory <gregory.oakes@outlook.com>
 * Author: Oakes, Gregory <gregory.oakes@outlook.com>
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#![warn(clippy::all)]

pub mod opcode;

mod cmd;
pub use cmd::*;

pub mod status;
pub use status::Status;

pub mod models;

mod device;
pub use device::*;

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("NVM command had an unsuccessful status: {0}")]
    NVMStatus(Status),
    #[error(transparent)]
    IOError(#[from] std::io::Error),
    #[error(transparent)]
    ParseError(#[from] models::FromBufError),
}

#[cfg(feature = "python")]
pub mod py;

#[cfg(feature = "python")]
pub use py::nvme_core;
