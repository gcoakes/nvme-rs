/*
 * Copyright (C) 2019  Oakes, Gregory <gregory.oakes@outlook.com>
 * Author: Oakes, Gregory <gregory.oakes@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use nvme_core::{
    models, models::TryFromBuf, opcode::AdminOpcode, CommandBuilder, Device, Error, FromDevice,
    FromNamespace, Status,
};
use serde_json;
use std::{
    io::{prelude::*, stdout},
    mem,
    path::PathBuf,
    process::exit,
    str::FromStr,
    time::Duration,
};
use structopt::StructOpt;

#[derive(Debug, PartialEq)]
enum OutputFormat {
    Json,
    Binary,
}

#[derive(Debug, StructOpt)]
#[structopt(name = "nvme-rs")]
struct Opt {
    #[structopt(subcommand)]
    cmd: Command,
}

impl FromStr for OutputFormat {
    type Err = clap::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "json" => Ok(OutputFormat::Json),
            "binary" => Ok(OutputFormat::Binary),
            "raw" => Ok(OutputFormat::Binary),
            x => Err(clap::Error::with_description(
                &format!("Cannot understand the given output format: {}", x),
                clap::ErrorKind::InvalidValue,
            )),
        }
    }
}

#[derive(Debug, StructOpt)]
enum Command {
    AdminPassthru {
        #[structopt(long, default_value = "0")]
        opcode: u8,
        #[structopt(long, default_value = "0")]
        flags: u8,
        #[structopt(long, default_value = "0")]
        nsid: u32,
        #[structopt(long, default_value = "0")]
        data_len: usize,
        #[structopt(long, default_value = "0")]
        cdw10: u32,
        #[structopt(long, default_value = "0")]
        cdw11: u32,
        #[structopt(long, default_value = "0")]
        cdw12: u32,
        #[structopt(long, default_value = "0")]
        cdw13: u32,
        #[structopt(long, default_value = "0")]
        cdw14: u32,
        #[structopt(long, default_value = "0")]
        cdw15: u32,
        #[structopt(long, default_value = "0")]
        timeout: u32,
        #[structopt(parse(from_os_str))]
        device: PathBuf,
    },
    GetLog {
        #[structopt(parse(from_os_str))]
        device: PathBuf,
        #[structopt(long = "namespace-id", short, default_value = "0")]
        nsid: u32,
        #[structopt(long = "log-id", short = "i")]
        lid: u8,
        #[structopt(long, short = "s", default_value = "0")]
        lsp: u8,
        #[structopt(long, short = "o", default_value = "0")]
        lpo: u64,
        #[structopt(long, default_value = "0")]
        lsi: u16,
        #[structopt(long, short)]
        rae: bool,
        #[structopt(long = "uuid-index", short = "U", default_value = "0")]
        uuid_ix: u8,
        #[structopt(long = "log-len", short)]
        data_len: usize,
    },
    IDCtrl {
        #[structopt(short, long, default_value = "json")]
        output: OutputFormat,
        #[structopt(parse(from_os_str))]
        device: PathBuf,
    },
    IDNs {
        #[structopt(short, long, default_value = "json")]
        output: OutputFormat,
        #[structopt(parse(from_os_str))]
        device: PathBuf,
        #[structopt(long, default_value = "1")]
        nsid: u32,
    },
    EffectsLog {
        #[structopt(short, long, default_value = "json")]
        output: OutputFormat,
        #[structopt(parse(from_os_str))]
        device: PathBuf,
    },
    FwLog {
        #[structopt(short, long, default_value = "json")]
        output: OutputFormat,
        #[structopt(parse(from_os_str))]
        device: PathBuf,
    },
    SmartLog {
        #[structopt(short, long, default_value = "json")]
        output: OutputFormat,
        #[structopt(parse(from_os_str))]
        device: PathBuf,
    },
    ErrorLog {
        #[structopt(short, long, default_value = "json")]
        output: OutputFormat,
        #[structopt(parse(from_os_str))]
        device: PathBuf,
        #[structopt(long, default_value = "0")]
        offset: usize,
        #[structopt(long, default_value = "64")]
        num_errors: usize,
    },
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();
    let (code, maybe_data): (i32, Option<Box<[u8]>>) = match opt.cmd {
        Command::AdminPassthru {
            opcode,
            flags,
            nsid,
            data_len,
            cdw10,
            cdw11,
            cdw12,
            cdw13,
            cdw14,
            cdw15,
            timeout,
            device,
        } => {
            let cmd = CommandBuilder::new(AdminOpcode::from(opcode))
                .with_flags(flags)
                .with_nsid(nsid)
                .with_data_len(data_len)
                .with_cdw10(cdw10)
                .with_cdw11(cdw11)
                .with_cdw12(cdw12)
                .with_cdw13(cdw13)
                .with_cdw14(cdw14)
                .with_cdw15(cdw15)
                .with_timeout(Duration::from_millis(timeout.into()));
            let mut dev = Device::open(device)?;
            let (status, maybe_data) = cmd.submit(&mut dev)?;
            (status.raw_code().into(), maybe_data)
        }
        Command::GetLog {
            device,
            nsid,
            lid,
            lsp,
            lpo,
            lsi,
            rae,
            uuid_ix,
            data_len,
        } => {
            let cmd = CommandBuilder::get_log_page(lid, lsp, lpo, lsi, rae, uuid_ix, data_len)
                .with_nsid(nsid);
            let mut dev = Device::open(device)?;
            let (status, maybe_data) = cmd.submit(&mut dev)?;
            (status.raw_code().into(), maybe_data)
        }
        Command::IDCtrl { device, output } => {
            let mut dev = Device::open(device)?;
            get_struct::<models::IDCtrl>(&mut dev, output)
        }
        Command::IDNs {
            device,
            output,
            nsid,
        } => {
            let mut dev = Device::open(device)?;
            get_struct_ns::<models::IDNamespace>(&mut dev, nsid, output)
        }
        Command::EffectsLog { device, output } => {
            let mut dev = Device::open(device)?;
            get_struct::<models::logpage::CmdEffectLog>(&mut dev, output)
        }
        Command::FwLog { device, output } => {
            let mut dev = Device::open(device)?;
            get_struct::<models::logpage::FwSlotLog>(&mut dev, output)
        }
        Command::SmartLog { device, output } => {
            let mut dev = Device::open(device)?;
            get_struct::<models::logpage::SmartLog>(&mut dev, output)
        }
        Command::ErrorLog {
            output,
            device,
            offset,
            num_errors,
        } => {
            let cmd = CommandBuilder::get_log_page(
                0x01,
                0,
                (offset * mem::size_of::<models::logpage::ErrorLogEntry>()) as u64,
                0,
                false,
                0,
                num_errors * mem::size_of::<models::logpage::ErrorLogEntry>(),
            );
            let mut dev = Device::open(device)?;
            let mut last_status: Option<Status> = None;
            // TODO: Refactor error page iterator to be more ergonomic.
            match output {
                OutputFormat::Json => {
                    let mut errs: Vec<models::logpage::ErrorLogEntry> = Vec::new();
                    for page_res in cmd.submit_get_log_iter(&mut dev) {
                        let (status, maybe_page) = page_res?;
                        if let Some(page) = maybe_page {
                            match <[models::logpage::ErrorLogEntry]>::try_from_buf(page) {
                                Ok(new_page) => errs.extend_from_slice(new_page.as_ref()),
                                _ => (),
                            }
                        }
                        last_status = Some(status);
                    }
                    (
                        last_status.map_or(0, |s| s.raw_code()),
                        Some(
                            serde_json::to_string_pretty(&errs)?
                                .into_bytes()
                                .into_boxed_slice(),
                        ),
                    )
                }
                OutputFormat::Binary => {
                    let mut errs: Vec<u8> = Vec::new();
                    for page_res in cmd.submit_get_log_iter(&mut dev) {
                        let (status, maybe_page) = page_res?;
                        if let Some(page) = maybe_page {
                            errs.extend_from_slice(page.as_ref());
                        }
                        last_status = Some(status);
                    }
                    (
                        last_status.map_or(0, |s| s.raw_code()),
                        Some(errs.into_boxed_slice()),
                    )
                }
            }
        }
    };
    if let Some(data) = maybe_data {
        stdout().lock().write_all(&data)?;
    }
    exit(code)
}

fn get_struct<T: FromDevice + serde::Serialize>(
    dev: &mut Device,
    output: OutputFormat,
) -> (i32, Option<Box<[u8]>>) {
    match output {
        OutputFormat::Json => match dev.query_status::<T>() {
            Ok((status, stru)) => (
                status.raw_code(),
                match serde_json::to_string_pretty(stru.as_ref()) {
                    Ok(s) => Some(s.into_bytes().into_boxed_slice()),
                    Err(_) => None,
                },
            ),
            Err(Error::NVMStatus(s)) => (s.raw_code(), None),
            Err(Error::IOError(e)) => match e.raw_os_error() {
                Some(code) => (code, None),
                _ => (-1, None),
            },
            _ => (-1, None),
        },
        OutputFormat::Binary => match dev.query_raw_status::<T>() {
            Ok((status, stru)) => (status.raw_code(), Some(stru)),
            Err(Error::NVMStatus(s)) => (s.raw_code(), None),
            Err(Error::IOError(e)) => match e.raw_os_error() {
                Some(code) => (code, None),
                _ => (-1, None),
            },
            _ => (-1, None),
        },
    }
}

fn get_struct_ns<T: FromNamespace + serde::Serialize>(
    dev: &mut Device,
    nsid: u32,
    output: OutputFormat,
) -> (i32, Option<Box<[u8]>>) {
    match output {
        OutputFormat::Json => match dev.query_ns_status::<T>(nsid) {
            Ok((status, stru)) => (
                status.raw_code(),
                match serde_json::to_string_pretty(stru.as_ref()) {
                    Ok(s) => Some(s.into_bytes().into_boxed_slice()),
                    Err(_) => None,
                },
            ),
            Err(Error::NVMStatus(s)) => (s.raw_code(), None),
            Err(Error::IOError(e)) => match e.raw_os_error() {
                Some(code) => (code, None),
                _ => (-1, None),
            },
            _ => (-1, None),
        },
        OutputFormat::Binary => match dev.query_ns_raw_status::<T>(nsid) {
            Ok((status, stru)) => (status.raw_code(), Some(stru)),
            Err(Error::NVMStatus(s)) => (s.raw_code(), None),
            Err(Error::IOError(e)) => match e.raw_os_error() {
                Some(code) => (code, None),
                _ => (-1, None),
            },
            _ => (-1, None),
        },
    }
}
